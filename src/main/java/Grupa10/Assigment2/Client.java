package Grupa10.Assigment2;

public class Client implements Comparable<Client>{
	private int ID;
	private int arrivalTime;
	private int processingTime;
	private int finalizare;
	private boolean procesat;
	public Client(int id, int arr, int pro)
	{
		this.ID=id;
		this.arrivalTime=arr;
		this.processingTime=pro;
		this.finalizare=0;
		this.procesat=false;
	}
	public synchronized void procesare()
	{
		this.processingTime--;
	}
	public void setArrivalTime(int arv)
	{
		this.arrivalTime=arv;
	}
	public void setProcessingTime(int pro)
	{
		this.processingTime=pro;
	}
	public void setID(int id)
	{
		this.ID=id;
	}
	public int getArrivalTime()
	{
		return arrivalTime;
	}
	public int getProcessingTime()
	{
		return processingTime;
	}
	public int getID()
	{
		return ID;
	}
	public int getTimpFinalizare()
	{
		return finalizare;
	}
	public void setTimpFinalizare(int timpFinal)
	{
		this.finalizare=timpFinal;
	}
	@Override
	public int compareTo(Client o) {
		return this.getArrivalTime()-o.getArrivalTime();
	}
	public void setProcesat(boolean proc)
	{
		this.procesat=proc;
	}
	public boolean isProcesat()
	{
		return procesat;
	}
	public String toString()
	{
		return "("+ID+","+arrivalTime+","+processingTime+")";
	}
	
	

}
