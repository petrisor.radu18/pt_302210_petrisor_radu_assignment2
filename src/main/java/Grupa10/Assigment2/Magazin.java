package Grupa10.Assigment2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.BlockingQueue;

public class Magazin {
	private ArrayList<Coada> cozi= new ArrayList<Coada>();
	private int nrCozi;
	public Magazin(int nrc)
	{
		this.nrCozi=nrc;
		int contor=1;
		while(contor<=nrCozi)
		{
			Coada c=new Coada();
			cozi.add(c);
			Thread t=new Thread(c);
			t.start();
			contor++;
		}
	}
	public void adaugareClientCoada(Client x)
	{ 
		int timpMinim=Integer.MAX_VALUE;
		for(Coada c:cozi)
		{
			if(c.getPerioadaDeAsteptare()<timpMinim)
			{
				timpMinim=c.getPerioadaDeAsteptare();
			}
		}
		for(Coada c:cozi)
		{
			if(c.getPerioadaDeAsteptare()==timpMinim)
			{
				c.adaugareClient(x);
			    break;
			}
		}
    
	}
	public void afisareCozi(FileWriter fr)
	{
		try {
			//File pf=new File(out);
			//FileWriter fr=new FileWriter(out);
		for(Coada c:cozi)
		{
			//System.out.print("Coada:");
			fr.write("Coada:");
			if((c.getClientiiDinCoada()).isEmpty())
			{//System.out.print("Inchisa");
				fr.write("Inchisa");}
			else {
		  //System.out.print(c.afisareCoada());
				fr.write(c.afisareCoada());
			}
			//System.out.println("");
			fr.write("\n");
	     }
		//System.out.println("");
		fr.write("\n");
		}
		catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();}
		
	}
	public void oprireThreadCoada()
	{
		for(Coada c:cozi)
		{
			c.opresteThread();
		}
	}
	public void seteazaTimpiiPentruCozi()
	{
		for(Coada c:cozi)
		{
			c.calcularePeriodaDeAsteptare();
		}
	}
	public ArrayList<Coada> getCozi()
	{
		return cozi;
	}

}
