package Grupa10.Assigment2;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Coada implements Runnable,Comparable<Coada>{
	private BlockingQueue<Client> clienti=new ArrayBlockingQueue<Client>(50);
	private AtomicInteger perioadaDeAsteptare;
	public boolean oprire;
	public Coada()
	{
		this.perioadaDeAsteptare=new AtomicInteger(0);
		oprire=true;
	}
	void adaugareClient(Client a)
	{
		perioadaDeAsteptare.set(perioadaDeAsteptare.get()+a.getProcessingTime());
		a.setTimpFinalizare(perioadaDeAsteptare.get());
		clienti.add(a);
	}
	
	@Override
	public void run() {
		
       while(oprire)
       {
         try {
    		  Client  x=clienti.take();
    		   x.setProcesat(true);
    		   clienti.add(x);
    		   Thread.sleep(x.getProcessingTime()*1000);
    		   clienti.poll();
    		   
    	   }
    	   catch(InterruptedException exceptie)
    	   {
    		   System.out.println("Exceptie de intrerupere la coada");
    	   }
       }	
	}
	public BlockingQueue<Client> getClientiiDinCoada()
	{
		return this.clienti;
	}
	public void setClientiiDinCoada(BlockingQueue<Client> c)
	{
		this.clienti=c;
	}
	public int getPerioadaDeAsteptare()
	{
		return perioadaDeAsteptare.get();
	}
	public void calcularePeriodaDeAsteptare()
	{
		int a=0;
		   for(Client x:clienti)
		    {
			  a=a+x.getProcessingTime();
		    }
          
      perioadaDeAsteptare.set(a);
	}

	public String afisareCoada()
	{
		String str="";
		   for(Client x:clienti)
		     {
			 str=str+x.toString()+" ";
		     }
		return str;
	}
	@Override
	public int compareTo(Coada o) {
		return this.perioadaDeAsteptare.get()-o.perioadaDeAsteptare.get();
	}
	public void opresteThread()
	{
		this.oprire=false;
	}

}
