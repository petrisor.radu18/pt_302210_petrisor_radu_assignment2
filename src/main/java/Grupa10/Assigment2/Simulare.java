package Grupa10.Assigment2;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Simulare implements Runnable{
	private int timpLimita;
	private int nrClienti;
	private int nrCozi;
	private int timpProcesareMinim;
	private int timpProcesareMaxim;
	private int timpArrivalMinim;
	private int timpArrivalMaxim;
	private String fisierIntrare;
	private String fisierIesire;
	private int timpMediuAsteptare=0;
	private Magazin magazin;
	private  ArrayList<Client> clientiMagazin=new ArrayList<Client>();
	private  ArrayList<Client> terminareClienti=new ArrayList<Client>();
	public Simulare(String fisierIntrare, String fisierIesire)
	{
		try {this.fisierIntrare=fisierIntrare;
			 this.fisierIesire= fisierIesire;
		    File fisierul=new File(fisierIntrare);
			Scanner scanner=new Scanner(fisierul);
			scanner.useDelimiter("\\D+");
			this.nrClienti=scanner.nextInt();
			this.nrCozi=scanner.nextInt();
			this.timpLimita=scanner.nextInt();
			this.timpArrivalMinim=scanner.nextInt();
			this.timpArrivalMaxim=scanner.nextInt();
			this.timpProcesareMinim=scanner.nextInt();
			this.timpProcesareMaxim=scanner.nextInt();
			scanner.close();
		} catch (FileNotFoundException e) {
			 System.out.println("Fisierul "+fisierIntrare+" nu a fost gasit");
			e.printStackTrace();
		}
		this.magazin=new Magazin(this.nrCozi);
		this.generareClientiRandom();
	}
	@Override
	public  void run() {
		int timpCurent=0;
		try {
		FileWriter fi=new FileWriter(this.fisierIesire);
		while(timpCurent<=this.timpLimita)	
		{   System.out.println("Timpul:"+timpCurent);
			fi.write("Timpul:"+timpCurent+"\n");
			   for(Client c:clientiMagazin)
			    {
			      if(c.getArrivalTime()==timpCurent)
			        {
				      magazin.adaugareClientCoada(c);
			        } 
			   }
			
			finalizareClientiCoada(timpCurent);
			//System.out.print("Clientii:");
			fi.write("Clientii:");
			for(Client c:clientiMagazin)
			{
				//System.out.print(c.toString()+" ");
				fi.write(c.toString()+" ");
			}
			//System.out.println("");
			fi.write("\n");
			magazin.seteazaTimpiiPentruCozi();
			for(Client cl:terminareClienti)
			{
				if(cl.getProcessingTime()>0)
				{
				if(cl.isProcesat()==true && cl.getArrivalTime()<=timpCurent && cl.getArrivalTime()>0)
				{
					cl.procesare();
				}}
			}
			magazin.afisareCozi(fi);
			timpCurent++;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {e.printStackTrace();}
			
		}
	         this.afisareTimpMediu(fi);
		     fi.close();
		 	magazin.oprireThreadCoada();    
		}
		 catch (IOException e1) {e1.printStackTrace();}
		}
	private float calculareTimpMediu()
	{
		int x;
		for(Client c:terminareClienti)
		{
	      this.timpMediuAsteptare=this.timpMediuAsteptare+c.getTimpFinalizare();
		}
		x=this.timpMediuAsteptare/this.nrClienti;
		return x;
	}
	private void afisareTimpMediu(FileWriter fr)
	{
		try {
			fr.write("Timpul Mediu de Asteptare:"+this.calculareTimpMediu()+"  secunde \n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void generareClientiRandom()
	{
		Random r=new Random();
		for(int i=1;i<=this.nrClienti;i++)
		{
		  int arrivalTime;
		  int processingTime;
		  arrivalTime=r.nextInt(this.timpArrivalMaxim-this.timpArrivalMinim)+this.timpArrivalMinim;
		  processingTime=r.nextInt(this.timpProcesareMaxim-this.timpProcesareMinim)+this.timpProcesareMinim;
		  this.clientiMagazin.add(new Client(i,arrivalTime,processingTime));
		}
		Collections.sort(clientiMagazin);
	}
	private void finalizareClientiCoada(int time)
	{
		while(true)
		{
			if(!clientiMagazin.isEmpty())
			{
				if(clientiMagazin.get(0).getArrivalTime()==time && clientiMagazin!=null)
				{
					terminareClienti.add(clientiMagazin.get(0));
					clientiMagazin.remove(0);
				}
				else break;
			}
			else break;
		}
	}
	public static void main(String[] args)
	{ if(args.length==6)
	{
		int i=0;
		while(i<6)
		{
		Simulare simulare1=new Simulare(args[i],args[i+1]);
		Thread t=new Thread(simulare1);
		t.start();
		i=i+2;
		}
	}
	else System.out.println("Programul se foloseste in felul urmator:nume_prog.jar <fisierintrare1.txt> <fisieriesire1.txt> <fisierintrare2.txt> <fisieriesire2.txt> <fisierintrare3.txt> <fisieriesire3.txt>");
		
	}

}
